package net.thumbtack.replicator;


import com.google.code.or.OpenReplicator;
import com.google.code.or.binlog.BinlogEventListener;
import com.google.code.or.binlog.BinlogEventV4;
import com.google.code.or.binlog.BinlogParserListener;
import net.thumbtack.replicator.common.*;
import net.thumbtack.replicator.exception.OpenReplicatorException;
import net.thumbtack.replicator.exception.ParseException;
import net.thumbtack.replicator.exception.StorageException;
import net.thumbtack.replicator.exception.TransformException;
import net.thumbtack.replicator.parser.EventParser;
import net.thumbtack.replicator.storage.KeyValueStorage;
import net.thumbtack.replicator.storage.impl.CouchbaseStorage;
import net.thumbtack.replicator.transformer.Transformer;
import net.thumbtack.replicator.transformer.impl.SimpleTransformer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;


/**
 * Parses MySQL binary log and replicates changes to key-value storages
 */
public class Replicator {

    private static final Logger LOG = LoggerFactory.getLogger(Replicator.class);
    private static final String CONFIG_FILE_NAME = "configuration.xml";

    private OpenReplicator openReplicator;
    private EventParser eventParser;
    private Set<KeyValueStorage> storages;
    private Transformer transformer;
    private Configuration configuration;


    public void init() throws StorageException, IOException, ParserConfigurationException, SAXException {

        configuration = new Configuration();
        configuration.load(CONFIG_FILE_NAME);
        MysqlConfiguration mysqlConfig = configuration.getMysqlConfig();
        CouchbaseConfiguration couchbaseConfig = configuration.getCouchbaseConfig();

        transformer = new SimpleTransformer();
        storages = new HashSet<>();
        CouchbaseStorage couchbaseStorage = new CouchbaseStorage(
                couchbaseConfig.getUri(),
                couchbaseConfig.getBucket(),
                couchbaseConfig.getPassword());
        storages.add(couchbaseStorage);
        eventParser = new EventParser(mysqlConfig.getTablesToReplicate());
        openReplicator = new BinlogParser();

        openReplicator.setUser(mysqlConfig.getUser());
        openReplicator.setPassword(mysqlConfig.getPassword());
        openReplicator.setHost(mysqlConfig.getHost());
        openReplicator.setPort(mysqlConfig.getPort());
        openReplicator.setServerId(mysqlConfig.getServerId());

        try {
            eventParser.restoreState();
            LOG.info("Replicator state's restored");
            String binlogFileName = eventParser.getBinlogFileName();
            openReplicator.setBinlogFileName(binlogFileName);
            long binlogPosition = eventParser.getBinlogPosition();
            openReplicator.setBinlogPosition(binlogPosition);
        } catch (Exception e) {
            LOG.warn("Can't restore state");
            String binlogFileName = mysqlConfig.getBinlogFileName();
            eventParser.setBinlogFileName(binlogFileName);
            openReplicator.setBinlogFileName(binlogFileName);
            long binlogStartPosition = mysqlConfig.getBinlogStartPosition();
            eventParser.setBinlogPosition(binlogStartPosition);
            openReplicator.setBinlogPosition(binlogStartPosition);
        }
        openReplicator.setBinlogEventListener(new BinlogEventListenerImpl(this));
    }

    public boolean isRunning() {
        return openReplicator.isRunning();
    }

    public void start() throws OpenReplicatorException {
        try {
            openReplicator.start();
        } catch (Exception e) {
            throw new OpenReplicatorException("Can't start", e);
        }
        BinlogParserListener parserListener = new BinlogParserListenerImpl(this);
        openReplicator.getBinlogParser().addParserListener(parserListener);

        LOG.info("Connected to MySQL: host=" + configuration.getMysqlConfig().getHost()
            + ":" + configuration.getMysqlConfig().getPort()
            + "; user=" + configuration.getMysqlConfig().getUser());
        LOG.info("Replicator started");
    }

    public void stop(long timeout, TimeUnit timeUnit) {

        for (KeyValueStorage storage : storages) {
            storage.shutdown();
        }
        LOG.info("All key-value storage's clients are shut down");
        try {
            if (openReplicator.isRunning()) {
                openReplicator.stop(timeout, timeUnit);
            }
        } catch (Exception e) {
            LOG.warn("Can't stop OpenReplicator");
        }
        LOG.info("Replicator stopped");
    }

    public void stop() {
        stop(0, TimeUnit.MILLISECONDS);
    }


//  ---------------------------------PRIVATE----------------------------------------------------------------------------

    private class BinlogEventListenerImpl implements BinlogEventListener {

        private final Replicator replicator;

        public BinlogEventListenerImpl(Replicator replicator) {
            this.replicator = replicator;
        }

        @Override
        public void onEvents(BinlogEventV4 event) {
            try {
                processEvent(event);
            } catch (ParseException e) {
                LOG.error("Error parsing: " + e.getMessage());
                replicator.stop();
            } catch (TransformException e) {
                LOG.error("Error transform: " + e.getMessage());
                replicator.stop();
            } catch (StorageException e) {
                LOG.error("Database error: " + e.getMessage());
                replicator.stop();
            } catch (XMLStreamException | IOException e) {
                LOG.warn("Error store replicator's state: " + e.getMessage());
            }
        }

        private void processEvent(BinlogEventV4 event) throws ParseException, IOException, TransformException, StorageException, XMLStreamException {

            List<Change> changes = eventParser.parse(event);
            LOG.debug("CHANGES: " + changes);
            if (changes == null || changes.isEmpty()) {
                eventParser.storeState();
                return;
            }
            for (Change change : changes) {
                replicateChange(change);
            }
            eventParser.storeState();
        }

        private void replicateChange(Change change) throws TransformException, StorageException {

            String key = change.getDatabaseName() + "." + change.getTableName() + "." + change.getPrimaryKey();
            switch (change.getType()) {
                case INSERT: {
                    LOG.info("Inserting object with key "
                            + MysqlTable.getUniqueName(change.getDatabaseName(), change.getTableName())
                            + "." + change.getPrimaryKey());
                    ByteBuffer dataToStore = transformer.transform(key, change.getColumnsData());
                    for (KeyValueStorage storage : storages) {
                        storage.set(key, dataToStore);
                    }
                    //LOG.debug("Getting object: " + new String(storage.get(key).array()));
                    break;
                }
                case UPDATE: {
                    LOG.info("Updating object with key "
                            + MysqlTable.getUniqueName(change.getDatabaseName(), change.getTableName())
                            + "." + change.getPrimaryKey());
                    ByteBuffer dataToStore = transformer.transform(key, change.getColumnsData());
                    for (KeyValueStorage storage : storages) {
                        storage.set(key, dataToStore);
                    }
                    break;
                }
                case DELETE: {
                    LOG.info("Removing object with key "
                            + MysqlTable.getUniqueName(change.getDatabaseName(), change.getTableName())
                            + "." + change.getPrimaryKey());
                    for (KeyValueStorage storage : storages) {
                        storage.remove(key);
                    }
                    break;
                }
                default:
                    LOG.error("Unknown change type " + change.getType());
            }
        }
    } // end of private class BinlogEventListenerImpl


    private class BinlogParserListenerImpl implements BinlogParserListener {

        private final Replicator replicator;

        public BinlogParserListenerImpl(Replicator replicator) {
            this.replicator = replicator;
        }

        @Override
        public void onStart(com.google.code.or.binlog.BinlogParser parser) {
        }

        @Override
        public void onStop(com.google.code.or.binlog.BinlogParser parser) {
        }

        @Override
        public void onException(com.google.code.or.binlog.BinlogParser parser, Exception exception) {
            LOG.error("Binlog parsing exception:", exception);
            replicator.stop();
        }
    }
}