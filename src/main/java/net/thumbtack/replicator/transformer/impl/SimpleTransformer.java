package net.thumbtack.replicator.transformer.impl;


import net.thumbtack.replicator.exception.TransformException;
import net.thumbtack.replicator.transformer.Transformer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Map;


public class SimpleTransformer implements Transformer {

    private static final Logger LOG = LoggerFactory.getLogger(SimpleTransformer.class);

    @Override
    public ByteBuffer transform(String key, Map<String, ByteBuffer> columnData) throws TransformException {
        ByteBuffer byteBuffer;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            bos.write('{');
            boolean isNeedSeparate = false;
            for (String column : columnData.keySet()) {
                if (isNeedSeparate) {
                    bos.write(',');
                } else {
                    isNeedSeparate = true;
                }
                bos.write(column.getBytes());
                bos.write('=');
                ByteBuffer columnValue = columnData.get(column);
                bos.write(columnValue.array());
            }
            bos.write('}');
            byteBuffer = ByteBuffer.wrap(bos.toByteArray());
        } catch (IOException ioe) {
            throw new TransformException(ioe);
        }
        return byteBuffer;
    }
}
