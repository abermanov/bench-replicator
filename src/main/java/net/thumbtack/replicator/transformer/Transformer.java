package net.thumbtack.replicator.transformer;


import net.thumbtack.replicator.exception.TransformException;

import java.nio.ByteBuffer;
import java.util.Map;


/**
 * Interface for collapsing column values into single value to be stored by key.
 * Implementation of this interface should be called on insert/update actions to select and
 * serialize columns data into single value
 */
public interface Transformer {

    ByteBuffer transform(String key, Map<String, ByteBuffer> columnData) throws TransformException;

}
