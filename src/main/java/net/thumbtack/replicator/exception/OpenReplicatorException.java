package net.thumbtack.replicator.exception;


public class OpenReplicatorException extends Exception {

    public OpenReplicatorException() {
        super();
    }

    public OpenReplicatorException(String message) {
        super(message);
    }

    public OpenReplicatorException(String message, Throwable cause) {
        super(message, cause);
    }

    public OpenReplicatorException(Throwable cause) {
        super(cause);
    }
}