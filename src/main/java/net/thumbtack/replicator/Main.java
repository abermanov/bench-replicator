package net.thumbtack.replicator;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;


public class Main {

    private static final Logger LOG = LoggerFactory.getLogger(Main.class);


    public static void main(String[] args) throws ParserConfigurationException, IOException, SAXException {

        final Replicator replicator = new Replicator();
        try {
            replicator.init();
            replicator.start();

            while (true) {
                if (!replicator.isRunning()) {
                    break;
                }
                Thread.sleep(1000);
            }
        } catch (Exception e) {
            LOG.error("FATAL ERROR", e);
        } finally {
            replicator.stop();
        }
    }
}
