package net.thumbtack.replicator.parser;


import com.google.code.or.binlog.BinlogEventV4;
import com.google.code.or.common.util.MySQLConstants;
import net.thumbtack.replicator.common.Change;
import net.thumbtack.replicator.common.MysqlTable;
import net.thumbtack.replicator.common.XmlErrorHandler;
import net.thumbtack.replicator.exception.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.*;
import java.util.*;


public class EventParser extends AbstractEventParser {

    private static final Logger LOG = LoggerFactory.getLogger(EventParser.class);

    private static final String STATE_FILE_NAME = "state.xml";

    private final Map<Integer, AbstractEventParser> parsers;

    public EventParser(Map<String, Set<String>> tablesToReplicate) {
        parsers = initParsers();
        this.tablesToReplicate = tablesToReplicate;
    }

    @Override
    public int getType() {
        return 0;
    }

    @Override
    public List<Change> doParse(BinlogEventV4 event) throws ParseException {

        LOG.debug("EVENT: " + event);

        int type =  event.getHeader().getEventType();
        AbstractEventParser parser = parsers.get(type);
        if (null == parser) {
            throw new ParseException("Unknown event type " + event.getClass());
        }
        List<Change> changes = parsers.get(type).parse(event);
        return changes;
    }

    public void storeState() throws IOException, XMLStreamException {

        OutputStream out = null;
        XMLStreamWriter writer = null;
        try {
            out = new FileOutputStream(STATE_FILE_NAME);
            XMLOutputFactory factory = XMLOutputFactory.newInstance();
            writer = factory.createXMLStreamWriter(out);
            storeState(writer);
        } finally {
            if (out != null) out.close();
            if (writer != null) writer.close();
        }
        LOG.debug("State is stored");
    }

    public void restoreState() throws IOException, ParserConfigurationException, SAXException, XPathExpressionException {

        Document doc;
        InputStream in = null;
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            factory.setIgnoringElementContentWhitespace(true);
            DocumentBuilder builder = factory.newDocumentBuilder();
            builder.setErrorHandler(new XmlErrorHandler());
            in = new FileInputStream(STATE_FILE_NAME);
            doc = builder.parse(in);
            restoreState(doc);
        } finally {
            if (in != null) in.close();
        }

        LOG.debug("State is restored");
    }

    public String getBinlogFileName() {
        return binlogFileName;
    }

    public void setBinlogFileName(String fileName) {
        binlogFileName = fileName;
    }

    public long getBinlogPosition() {
        return binlogPosition;
    }

    public void setBinlogPosition(long position) {
        binlogPosition = position;
    }

    private Map<Integer, AbstractEventParser> initParsers() {

        Map<Integer, AbstractEventParser> parsers = new HashMap<>();
        parsers.put(MySQLConstants.QUERY_EVENT, new QueryEventParser());
        parsers.put(MySQLConstants.WRITE_ROWS_EVENT, new WriteRowsEventParser());
        parsers.put(MySQLConstants.UPDATE_ROWS_EVENT, new UpdateRowsEventParser());
        parsers.put(MySQLConstants.TABLE_MAP_EVENT, new TableMapEventParser());
        parsers.put(MySQLConstants.DELETE_ROWS_EVENT, new DeleteRowsEventParser());
        parsers.put(MySQLConstants.ROTATE_EVENT, new RotateEventParser());
        return parsers;
    }

    private void storeState(final XMLStreamWriter writer) throws XMLStreamException {
        writer.writeStartDocument();
        writer.writeStartElement("parser");

        writer.writeStartElement("binlog");
        writer.writeStartElement("filename");
        writer.writeCharacters(String.valueOf(binlogFileName));
        writer.writeEndElement();
        writer.writeStartElement("position");
        writer.writeCharacters(String.valueOf(binlogPosition));
        writer.writeEndElement();
        writer.writeEndElement();

        for (MysqlTable table : mysqlTables.values()) {
            writer.writeStartElement("mysqlTable");
            writer.writeStartElement("databaseName");
            writer.writeCharacters(table.getDatabaseName());
            writer.writeEndElement();
            writer.writeStartElement("tableName");
            writer.writeCharacters(table.getTableName());
            writer.writeEndElement();
            writer.writeStartElement("columns");
            for (String column : table.getColumns()) {
                writer.writeStartElement("column");
                writer.writeCharacters(column);
                writer.writeEndElement();
            }
            writer.writeEndElement();
            writer.writeStartElement("primaryKey");
            writer.writeCharacters(table.getPrimaryKey());
            writer.writeEndElement();
            writer.writeStartElement("pkColumnPositions");
            for (int pos : table.getPkColumnPositions()) {
                writer.writeStartElement("position");
                writer.writeCharacters(String.valueOf(pos));
                writer.writeEndElement();
            }
            writer.writeEndElement();

            writer.writeEndElement();
        }

        writer.writeStartElement("idNameMap");
        for (long id : idNameMap.keySet()) {
            writer.writeStartElement("item");
            writer.writeStartElement("key");
            writer.writeCharacters(String.valueOf(id));
            writer.writeEndElement();
            writer.writeStartElement("value");
            writer.writeCharacters(idNameMap.get(id));
            writer.writeEndElement();
            writer.writeEndElement();
        }
        writer.writeEndElement();

        writer.writeEndElement();
        writer.writeEndDocument();
    }

    private void restoreState(Document doc) throws XPathExpressionException {
        XPathFactory xPathFactory = XPathFactory.newInstance();
        XPath xPath = xPathFactory.newXPath();

        binlogFileName = xPath.evaluate("/parser/binlog/filename", doc);
        binlogPosition = Long.parseLong(xPath.evaluate("/parser/binlog/position", doc));

        int tablesCount = Integer.parseInt(xPath.evaluate("count(/parser/mysqlTable)", doc));
        for (int i = 1; i <= tablesCount; i++) {
            MysqlTable mysqlTable = new MysqlTable();
            String path = "/parser/mysqlTable[" + i + "]";
            String databaseName = xPath.evaluate(path + "/databaseName", doc);
            mysqlTable.setDatabaseName(databaseName);
            String tableName = xPath.evaluate(path + "/tableName", doc);
            mysqlTable.setTableName(tableName);
            int columnsCount = Integer.parseInt(xPath.evaluate("count(" + path + "/columns/column)", doc));
            List<String> columns = new ArrayList<>();
            for (int j = 1; j <= columnsCount; j++) {
                String columnPath = path + "/columns/column[" + j + "]";
                String column = xPath.evaluate(columnPath, doc);
                columns.add(column);
            }
            mysqlTable.setColumns(columns);
            String primaryKey = xPath.evaluate(path + "/primaryKey", doc);
            mysqlTable.setPrimaryKey(primaryKey);
            int posCount = Integer.parseInt(xPath.evaluate("count(" + path + "/pkColumnPositions/position)", doc));
            Set<Integer> pkColumnPositions = new HashSet<>();
            for (int j = 1; j <= posCount; j++) {
                String posPath = path + "/pkColumnPositions/position[" + j + "]";
                int pos = Integer.parseInt(xPath.evaluate(posPath, doc));
                pkColumnPositions.add(pos);
            }
            mysqlTable.setPkColumnPositions(pkColumnPositions);
            addMysqlTable(mysqlTable);
        }

        int items = Integer.parseInt(xPath.evaluate("count(/parser/idNameMap/item)", doc));
        for (int i = 1; i <= items; i++) {
            String path = "/parser/idNameMap/item[" + i + "]";
            long id = Long.parseLong(xPath.evaluate(path + "/key", doc));
            String name = xPath.evaluate(path + "/value", doc);
            idNameMap.put(id, name);
        }
    }
}
