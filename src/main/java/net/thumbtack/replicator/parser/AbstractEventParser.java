package net.thumbtack.replicator.parser;


import com.google.code.or.binlog.BinlogEventV4;
import com.google.code.or.binlog.BinlogEventV4Header;
import net.thumbtack.replicator.common.Change;
import net.thumbtack.replicator.common.MysqlTable;
import net.thumbtack.replicator.exception.ParseException;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * Basic class for all event parsers
 */
public abstract class AbstractEventParser {

    protected static Map<Long, String> idNameMap = new HashMap<>();
    protected static String binlogFileName;
    protected static long binlogPosition;
    protected static Map<String, MysqlTable> mysqlTables = new HashMap<>();
    protected static Map<String, Set<String>> tablesToReplicate;


    public MysqlTable getMysqlTable(String name) {
        return mysqlTables.get(name);
    }

    public MysqlTable getMysqlTable(long id) {
        String name = idNameMap.get(id);
        return mysqlTables.get(name);
    }

    public void addMysqlTable(MysqlTable table) {
        mysqlTables.put(table.getUniqueName(), table);
    }

    public void setIdForTable(long id, String name) {
        idNameMap.put(id, name);
    }

    abstract public int getType();
    abstract protected List<Change> doParse(BinlogEventV4 eventV4) throws ParseException;

    final public List<Change> parse(BinlogEventV4 event) throws ParseException {
        BinlogEventV4Header header = event.getHeader();
        binlogPosition = header.getNextPosition();
        return doParse(event);
    }


    protected byte[] serialize(Object obj) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream os = new ObjectOutputStream(baos);
        os.writeObject(obj);
        return baos.toByteArray();
    }

    protected boolean isReplicated(String databaseName, String tableName) {
        Set<String> tables = tablesToReplicate.get(databaseName);
        if (tables == null) {
            return false;
        }
        if (tables.isEmpty()) {
            return true;
        }
        return tables.contains(tableName);
    }
}
