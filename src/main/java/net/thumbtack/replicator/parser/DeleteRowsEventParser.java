package net.thumbtack.replicator.parser;


import com.google.code.or.binlog.BinlogEventV4;
import com.google.code.or.binlog.impl.event.DeleteRowsEvent;
import com.google.code.or.common.glossary.Row;
import com.google.code.or.common.util.MySQLConstants;
import net.thumbtack.replicator.common.Change;
import net.thumbtack.replicator.common.ChangeType;
import net.thumbtack.replicator.common.MysqlTable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;


/**
 * Parser for 'delete rows' event
 */
public class DeleteRowsEventParser extends AbstractEventParser {

    private static final Logger LOG = LoggerFactory.getLogger(DeleteRowsEventParser.class);

    private static final int TYPE = MySQLConstants.DELETE_ROWS_EVENT;

    @Override
    public int getType() {
        return TYPE;
    }


    // tableId=34,reserved=1,columnCount=3,usedColumns=111,rows=[Row[columns=[4, 444, hello4]]
    @Override
    public List<Change> doParse(BinlogEventV4 eventV4) {
        LOG.debug("Parsing DeleteRowsEvent");
        DeleteRowsEvent event = (DeleteRowsEvent) eventV4;

        long tableId = event.getTableId();
        MysqlTable mysqlTable = getMysqlTable(tableId);
        if (mysqlTable == null) {
            return null;
        }
        Set<Integer> pkPositions = mysqlTable.getPkColumnPositions();
        List<Change> changes = new ArrayList<>();
        List<Row> rows = event.getRows();
        for (Row row : rows) {
            Change change = new Change(ChangeType.DELETE);
            change.setDatabaseName(mysqlTable.getDatabaseName());
            change.setTableName(mysqlTable.getTableName());
            StringBuilder keyBuilder = new StringBuilder();
            for (int pkPosition : pkPositions) {
                if (keyBuilder.length() != 0) keyBuilder.append('.');
                keyBuilder.append(row.getColumns().get(pkPosition).toString());
            }
            String keyValue = keyBuilder.toString();
            change.setPrimaryKey(keyValue);
            changes.add(change);
        }
        return changes;
    }
}
