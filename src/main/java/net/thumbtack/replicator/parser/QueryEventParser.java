package net.thumbtack.replicator.parser;


import com.foundationdb.sql.StandardException;
import com.foundationdb.sql.parser.*;
import com.google.code.or.binlog.BinlogEventV4;
import com.google.code.or.binlog.impl.event.QueryEvent;
import com.google.code.or.common.util.MySQLConstants;
import net.thumbtack.replicator.common.Change;
import net.thumbtack.replicator.common.MysqlTable;
import net.thumbtack.replicator.exception.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;


public class QueryEventParser extends AbstractEventParser {

    private static final Logger LOG = LoggerFactory.getLogger(QueryEventParser.class);

    private static final int TYPE = MySQLConstants.QUERY_EVENT;

    @Override
    public int getType() {
        return TYPE;
    }

    @Override
    public List<Change> doParse(BinlogEventV4 event) throws ParseException {

        QueryEvent queryEvent = (QueryEvent) event;

        //byte[] sql = queryEvent.getSql().getValue();
        String sql = queryEvent.getSql().toString();
        LOG.debug("SQL: " + sql);

        StatementNode stmt;
        try {
            SQLParser parser = new SQLParser();
            stmt = parser.parseStatement(sql);
        } catch (StandardException e) {
            throw new ParseException("SQL = " + sql);
        }

        int nodeType = stmt.getNodeType();
        switch (nodeType) {
            case NodeTypes.CREATE_TABLE_NODE: parseCreateTable(stmt);
            default: return null;
        }
    }


    private void parseCreateTable(StatementNode stmt) {

        CreateTableNode createTableNode = (CreateTableNode) stmt;
        TableName tableName = createTableNode.getObjectName();
        String dbName = tableName.getSchemaName();
        String table = tableName.getTableName();
        if (!isReplicated(dbName, table)) {
            return;
        }
        MysqlTable mysqlTable = new MysqlTable();
        mysqlTable.setDatabaseName(dbName);
        mysqlTable.setTableName(table);
        List<String> columns = new ArrayList<>();

        TableElementList tableElementList = createTableNode.getTableElementList();
        for (int i = 0; i < tableElementList.size(); i++) {

            TableElementNode tableElementNode = tableElementList.get(i);
            int nodeType = tableElementNode.getNodeType();

            if (nodeType == NodeTypes.COLUMN_DEFINITION_NODE) {
                columns.add(tableElementNode.getName());
            } else
            if (nodeType == NodeTypes.CONSTRAINT_DEFINITION_NODE) {
                ConstraintDefinitionNode constraintDefinitionNode = (ConstraintDefinitionNode) tableElementNode;
                if (constraintDefinitionNode.getConstraintType() != ConstraintDefinitionNode.ConstraintType.PRIMARY_KEY) {
                    continue;
                }
                ResultColumnList resultColumnList = constraintDefinitionNode.getColumnList();
                StringBuilder stringBuilder = new StringBuilder();
                for (int j = 0; j < resultColumnList.size(); j++) {
                    ResultColumn resultColumn = resultColumnList.get(j);
                    mysqlTable.addPkColumnPosition(resultColumn.getColumnPosition() - 1);
                    if (j != 0) stringBuilder.append('.');
                    stringBuilder.append(resultColumn.getName());
                }
                mysqlTable.setPrimaryKey(stringBuilder.toString());
            }
        }
        mysqlTable.setColumns(columns);
        addMysqlTable(mysqlTable);
        LOG.info("Found database table = " + mysqlTable);
    }
}
