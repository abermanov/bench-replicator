package net.thumbtack.replicator.parser;


import com.google.code.or.binlog.BinlogEventV4;
import com.google.code.or.binlog.impl.event.RotateEvent;
import com.google.code.or.common.util.MySQLConstants;
import net.thumbtack.replicator.common.Change;
import net.thumbtack.replicator.exception.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;


/**
 * Parser for RotateEvent. RotateEvent occurs when MySQL changes binary log file
 */
public class RotateEventParser extends AbstractEventParser {

    private static final int TYPE = MySQLConstants.ROTATE_EVENT;

    private static final Logger LOG = LoggerFactory.getLogger(RotateEventParser.class);

    @Override
    public  int getType() {
        return TYPE;
    }

    @Override
    public List<Change> doParse(BinlogEventV4 eventV4) throws ParseException {

        RotateEvent event = (RotateEvent) eventV4;
        binlogFileName = event.getBinlogFileName().toString();
        binlogPosition = event.getBinlogPosition();
        return null;
    }
}
