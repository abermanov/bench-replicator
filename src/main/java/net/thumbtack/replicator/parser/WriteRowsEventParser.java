package net.thumbtack.replicator.parser;


import com.google.code.or.binlog.BinlogEventV4;
import com.google.code.or.binlog.impl.event.WriteRowsEvent;
import com.google.code.or.common.glossary.Column;
import com.google.code.or.common.glossary.Row;
import com.google.code.or.common.glossary.column.BitColumn;
import com.google.code.or.common.util.MySQLConstants;
import net.thumbtack.replicator.common.Change;
import net.thumbtack.replicator.common.ChangeType;
import net.thumbtack.replicator.common.MysqlTable;
import net.thumbtack.replicator.exception.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.*;


/**
 * Parser for 'insert rows' event
 */

public class WriteRowsEventParser extends AbstractEventParser {

    private static final Logger LOG = LoggerFactory.getLogger(WriteRowsEventParser.class);

    private static final int TYPE = MySQLConstants.WRITE_ROWS_EVENT;


    @Override
    public int getType() {
        return TYPE;
    }

    @Override
    public List<Change> doParse(BinlogEventV4 eventV4) throws ParseException {

        WriteRowsEvent event = (WriteRowsEvent) eventV4;

        long tableId = event.getTableId();
        MysqlTable mysqlTable = getMysqlTable(tableId);
        if (mysqlTable == null) {
            return null;
        }

        List<Change> changes = new ArrayList<>();
        List<Row> rows = event.getRows();
        BitColumn usedColumns = event.getUsedColumns();
        Set<Integer> pkPositions = mysqlTable.getPkColumnPositions();

        for (Row row : rows) {

            Change change = new Change(ChangeType.INSERT);
            change.setDatabaseName(mysqlTable.getDatabaseName());
            change.setTableName(mysqlTable.getTableName());

            Map<String, ByteBuffer> columnsData = new HashMap<>();
            StringBuilder pkBuilder = new StringBuilder();
            int i = 0;
            for (Column column : row.getColumns()) {
                String columnName = mysqlTable.getColumns().get(i);
                if (usedColumns.get(i)) {
                    byte[] bytes;
                    try {
                        bytes = serialize(column.getValue());
                    } catch (IOException ioe) {
                        throw new ParseException("Serialization error", ioe);
                    }
                    ByteBuffer columnData = ByteBuffer.wrap(bytes);
                    columnsData.put(columnName, columnData);
                    if (pkPositions.contains(i)) {
                        if (pkBuilder.length() != 0) pkBuilder.append('.');
                        pkBuilder.append(column.toString());
                    }
                } else {
                    columnsData.put(columnName, null);
                }
                i++;
            }
            change.setColumnsData(columnsData);
            change.setPrimaryKey(pkBuilder.toString());
            changes.add(change);
        }
        return changes;
    }
}
