package net.thumbtack.replicator.parser;


import com.google.code.or.binlog.BinlogEventV4;
import com.google.code.or.binlog.impl.event.TableMapEvent;
import com.google.code.or.common.util.MySQLConstants;
import net.thumbtack.replicator.common.Change;
import net.thumbtack.replicator.common.MysqlTable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;


/**
 * Parser for TableMapEvent. TableMapEvent occurs when an association between table name and some id is found
 */
public class TableMapEventParser extends AbstractEventParser {

    private static final Logger LOG = LoggerFactory.getLogger(TableMapEventParser.class);

    private static final int TYPE = MySQLConstants.TABLE_MAP_EVENT;

    @Override
    public int getType() {
        return TYPE;
    }

    @Override
    public List<Change> doParse(BinlogEventV4 eventV4) {
        TableMapEvent event = (TableMapEvent) eventV4;
        String dbName = event.getDatabaseName().toString();
        String tableName = event.getTableName().toString();
        if (!isReplicated(dbName, tableName)) {
            return null;
        }
        String name = MysqlTable.getUniqueName(dbName, tableName);
        setIdForTable(event.getTableId(), name);
        return null;
    }
}
