package net.thumbtack.replicator.common;


import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Contains all configuration parameters for Replicator
 */
public class Configuration {

    private final MysqlConfiguration mysqlConfig = new MysqlConfiguration();
    private final CouchbaseConfiguration couchbaseConfig = new CouchbaseConfiguration();

    public void load(String fileName) throws ParserConfigurationException, IOException, SAXException {

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setValidating(true);
        factory.setIgnoringElementContentWhitespace(true);
        DocumentBuilder builder = factory.newDocumentBuilder();
        builder.setErrorHandler(new XmlErrorHandler());

        Document doc;
        InputStream in = null;
        try {
            in = new FileInputStream(fileName);
            doc = builder.parse(in);
        } finally {
            if (in != null) in.close();
        }
        Element configurationElement = doc.getDocumentElement();
        NodeList childrenConfiguration = configurationElement.getChildNodes();

        Element mysqlElement = (Element) childrenConfiguration.item(0);
        NodeList childrenMysql = mysqlElement.getChildNodes();
        Element userElement = (Element) childrenMysql.item(0);
        Text userText = (Text) userElement.getFirstChild();
        mysqlConfig.setUser(userText.getData());

        Element passwordElement = (Element) childrenMysql.item(1);
        Text passwordText = (Text) passwordElement.getFirstChild();
        mysqlConfig.setPassword(passwordText.getData());

        Element hostElement = (Element) childrenMysql.item(2);
        Text hostText = (Text) hostElement.getFirstChild();
        mysqlConfig.setHost(hostText.getData());

        Element portElement = (Element) childrenMysql.item(3);
        Text portText = (Text) portElement.getFirstChild();
        mysqlConfig.setPort(Integer.parseInt(portText.getData()));

        Element serverIdElement = (Element) childrenMysql.item(4);
        Text serverIdText = (Text) serverIdElement.getFirstChild();
        mysqlConfig.setServerId(Integer.parseInt(serverIdText.getData()));

        Element binlogElement = (Element) childrenMysql.item(5);
        NodeList childrenBinlog = binlogElement.getChildNodes();
        Element fileNameElement = (Element) childrenBinlog.item(0);
        Text fileNameText = (Text) fileNameElement.getFirstChild();
        mysqlConfig.setBinlogFileName(fileNameText.getData());
        Element startPositionElement = (Element) childrenBinlog.item(1);
        Text startPositionText = (Text) startPositionElement.getFirstChild();
        mysqlConfig.setBinlogStartPosition(Long.parseLong(startPositionText.getData()));

        Map<String, Set<String>> tablesToReplicate = new HashMap<>();
        for (int i = 6; i < childrenMysql.getLength(); i++) {
            Element databaseElement = (Element) childrenMysql.item(i);
            NodeList childrenDatabase = databaseElement.getChildNodes();
            Element nameElement = (Element) childrenDatabase.item(0);
            Text nameText = (Text) nameElement.getFirstChild();
            String dbName = nameText.getData();
            Set<String> tables = new HashSet<>();
            for (int j = 1; j < childrenDatabase.getLength(); j++) {
                Element tableElement = (Element) childrenDatabase.item(j);
                Text tableText = (Text) tableElement.getFirstChild();
                String table = tableText.getData();
                tables.add(table);
            }
            tablesToReplicate.put(dbName, tables);
        }
        mysqlConfig.setTablesToReplicate(tablesToReplicate);

        Element couchbaseElement = (Element) childrenConfiguration.item(1);
        NodeList childrenCouchbase = couchbaseElement.getChildNodes();
        Element uriElement = (Element) childrenCouchbase.item(0);
        Text uriText = (Text) uriElement.getFirstChild();
        couchbaseConfig.setUri(uriText.getData());

        Element bucketElement = (Element) childrenCouchbase.item(1);
        Text bucketText = (Text) bucketElement.getFirstChild();
        couchbaseConfig.setBucket(bucketText.getData());

        passwordElement = (Element) childrenCouchbase.item(2);
        passwordText = (Text) passwordElement.getFirstChild();
        String password = passwordText == null ? "" : passwordText.getData();
        couchbaseConfig.setPassword(password);
    }

    public MysqlConfiguration getMysqlConfig() {
        return mysqlConfig;
    }

    public CouchbaseConfiguration getCouchbaseConfig() {
        return couchbaseConfig;
    }
}
