package net.thumbtack.replicator.common;


import java.nio.ByteBuffer;
import java.util.Map;


/**
 * Describes some change (insert/update/delete) of a MySQL row
 */
public class Change {

    private final ChangeType type;

    private Map<String, ByteBuffer> columnsData;
    private String databaseName;
    private String tableName;
    private String primaryKey;

    public Change(final ChangeType type) {
        this.type = type;
    }

    public ChangeType getType() {
        return type;
    }

    public void setColumnsData(Map<String, ByteBuffer> columnsData) {
        this.columnsData = columnsData;
    }

    public Map<String, ByteBuffer> getColumnsData() {
        return columnsData;
    }

    public String getDatabaseName() {
        return databaseName;
    }

    public void setDatabaseName(String databaseName) {
        this.databaseName = databaseName;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getPrimaryKey() {
        return primaryKey;
    }

    public void setPrimaryKey(String primaryKey) {
        this.primaryKey = primaryKey;
    }

    @Override
    public String toString() {
        return "{type=" + type
                + "; databaseName=" + databaseName
                + "; tableName=" + tableName
                + "; primaryKey=" + primaryKey
                + "; columnsData=" + columnsData
                + "}";
    }
}
