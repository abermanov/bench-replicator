package net.thumbtack.replicator.common;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesLoader {

    private static final Logger LOG = LoggerFactory.getLogger(PropertiesLoader.class);

    public static Properties getProperties(String fileName) throws IOException {
        return getPropsFromFile(getDefaultProps(fileName), fileName);
    }

    private static Properties getPropsFromFile(Properties defaultProps, String fileName) {
        Properties props;
        if (defaultProps != null) {
            props = new Properties(defaultProps);
        } else {
            props = new Properties();
        }
        try {
            props.load(new FileInputStream(fileName));
        } catch (IOException ioe) {
            LOG.warn("Can't find file properties '" + fileName + "'. Default properties will be used");
        }
        return props;
    }

    private static Properties getDefaultProps(String fileName) throws IOException {
        Properties props = new Properties();
        InputStream in = PropertiesLoader.class.getResourceAsStream("/" + fileName);
        props.load(in);
        return props;
    }
}
