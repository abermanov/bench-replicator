package net.thumbtack.replicator.common;


import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

/**
 * Implementation of interface for SAX error handler
 */
public class XmlErrorHandler  implements ErrorHandler {

    @Override
    public void warning(SAXParseException exception) throws SAXException {
    }

    @Override
    public void error(SAXParseException exception) throws SAXException {
        throw new SAXException();
    }

    @Override
    public void fatalError(SAXParseException exception) throws SAXException {
        throw new SAXException();
    }
}