package net.thumbtack.replicator.common;

/**
 * Contains configuration parameters for Couchbase
 */
public class CouchbaseConfiguration {

    private String uri;
    private String bucket;
    private String password;

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getBucket() {
        return bucket;
    }

    public void setBucket(String bucket) {
        this.bucket = bucket;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
