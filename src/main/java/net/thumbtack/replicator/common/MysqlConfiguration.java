package net.thumbtack.replicator.common;

import java.util.Map;
import java.util.Set;


/**
 * Contains configuration parameters for MySQL
 */
public class MysqlConfiguration {

    private String user;
    private String password;
    private String host;
    private int port;
    private int serverId;
    private String binlogFileName;
    private long binlogStartPosition;
    private Map<String, Set<String>> tablesToReplicate;

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public int getServerId() {
        return serverId;
    }

    public void setServerId(int serverId) {
        this.serverId = serverId;
    }

    public String getBinlogFileName() {
        return binlogFileName;
    }

    public void setBinlogFileName(String binlogFileName) {
        this.binlogFileName = binlogFileName;
    }

    public long getBinlogStartPosition() {
        return binlogStartPosition;
    }

    public void setBinlogStartPosition(long binlogStartPosition) {
        this.binlogStartPosition = binlogStartPosition;
    }

    public Map<String, Set<String>> getTablesToReplicate() {
        return tablesToReplicate;
    }

    public void setTablesToReplicate(Map<String, Set<String>> tablesToReplicate) {
        this.tablesToReplicate = tablesToReplicate;
    }
}
