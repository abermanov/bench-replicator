package net.thumbtack.replicator.storage.impl;


import com.couchbase.client.CouchbaseClient;
import net.thumbtack.replicator.exception.StorageException;
import net.thumbtack.replicator.storage.KeyValueStorage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URI;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.List;

/**
 * Implementation of KeyValueStorage interface for Couchbase
 */
public class CouchbaseStorage implements KeyValueStorage {

    private static final Logger LOG = LoggerFactory.getLogger(CouchbaseStorage.class);
    private static final String PROPS_FILE_NAME = "keyValueStorage.properties";

    private CouchbaseClient client;

    public CouchbaseStorage(String uri, String bucket, String password) throws StorageException {
        try {
            List<URI> hosts = Arrays.asList(new URI(uri));
            client = new CouchbaseClient(hosts, bucket, password);
            LOG.info("Connected to Couchbase: hosts=" + hosts + "; bucket=" + bucket);
        } catch (Exception e) {
            throw new StorageException(e);
        }
    }

    @Override
    public ByteBuffer get(String key) throws StorageException {
        Object data;
        try {
            data = client.get(key);
        } catch (Exception e) {
            throw new StorageException(e);
        }
        //return toByteBuffer(data);
        byte[] bytes = (byte[])data;
        return ByteBuffer.wrap(bytes);
    }

    @Override
    public void set(String key, ByteBuffer data) throws StorageException {
        try {
            client.set(key, data.array()).get();
        } catch (Exception e) {
            throw new StorageException(e);
        }
    }

    @Override
    public void remove(String key) throws StorageException {
        try {
            client.delete(key).get();
        } catch (Exception e) {
            throw new StorageException(e);
        }
    }

    @Override
    public void shutdown() {
        client.shutdown();
        LOG.info("Shut down Couchbase client");
    }
}
